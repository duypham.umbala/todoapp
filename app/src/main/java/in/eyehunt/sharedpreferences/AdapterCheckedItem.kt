package `in`.eyehunt.sharedpreferences


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView


class AdapterCheckedItem() : RecyclerView.Adapter<AdapterCheckedItem.ViewHolder>() {

    var valuesChecked: List<String> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var listenerChecked: OnClickItemClickListener? = null

    override fun getItemCount() = valuesChecked.size

    override fun onCreateViewHolder(p0: ViewGroup, vp1: Int): AdapterCheckedItem.ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.checked_layout, p0, false)
        return ViewHolder(v)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textView: CheckBox? = null

        init {
            textView = itemView.findViewById(R.id.itemCheckedRadio)
        }

        fun bindDataChecked(item: String) {
            textView!!.text = item
            textView!!.setOnClickListener {
                listenerChecked?.onClickCheckedItem(item)
                listenerChecked?.onClickUnCheckedItem(item)
                textView!!.isChecked = true

            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindDataChecked(valuesChecked[position])
    }

}