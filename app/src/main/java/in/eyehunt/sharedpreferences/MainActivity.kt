package `in`.eyehunt.sharedpreferences


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(), OnClickItemClickListener {

    val adapterUnChecked = CustomAdapter()
    val adapterChecked = AdapterCheckedItem()
    private lateinit var realm: Realm
    private val TAG = MainActivity::class.java.simpleName
    private val viewChecked: TextView
        get() = findViewById(R.id.textView2)
    private val line : ImageView
        get() = findViewById(R.id.lineImage)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.commitTransaction()
        adapterUnChecked.listener = this
        adapterChecked.listenerChecked = this
        recycler.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = adapterUnChecked
        }
        recyclerChecked.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = adapterChecked
        }

        readData()
        inputItem.setOnClickListener {
            saveData(ModelList(inputItem.text.toString(), false))
            inputItem.text = null
            readData()
        }
        backHome.setOnClickListener {
            val intentHome: Intent = Intent(this@MainActivity, Home::class.java)
            startActivity(intentHome)
        }
    }


    private fun saveData(item: ModelList) {
        realm.executeTransaction {
            it.insertOrUpdate(item)
        }
    }

    fun getData(realmInstance: Realm): List<ModelList> {
        return realmInstance.where(ModelList::class.java).findAll()
    }


    @SuppressLint("SetTextI18n")
    private fun readData() {
        val listUnChecked = mutableListOf<String>()
        val listChecked = mutableListOf<String>()

        getData(realm).forEach {
            if (it.isPick!!) {
                listChecked.add(it.item!!)
            } else {
                listUnChecked.add(it.item!!)
            }
        }

        adapterUnChecked.values = listUnChecked
        adapterChecked.valuesChecked = listChecked
        if (listChecked.size <= 1) {
            when (listChecked.size) {
                0 -> {
                    line.visibility=View.GONE
                    viewChecked.visibility= View.GONE
                }
                1 -> {
                    line.visibility= VISIBLE
                    viewChecked.text = "1 Checked Item"
                    viewChecked.visibility= VISIBLE
                }
            }
        } else {
            line.visibility= VISIBLE
            viewChecked.text = "${listChecked.size}" + " Checked Items"
        }
    }


    override fun onClickRemveItem(item: String) {
        val result = realm.where(ModelList::class.java).equalTo("item", item).findAll()
        realm.executeTransaction {
            result.deleteAllFromRealm()
        }
        readData()
    }

    override fun onClickUnCheckedItem(item: String) {
        saveData(ModelList(item, false))
        readData()
    }

    override fun onClickCheckedItem(item: String) {
        saveData(ModelList(item, true))
        readData()
    }

    fun showPopup(view: View) {
        val popupMenu = PopupMenu(this, view)
        val inflater = popupMenu.menuInflater
        inflater.inflate(R.menu.popup_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.header1 ->
                    Toast.makeText(this, item.title, Toast.LENGTH_LONG).show()
                R.id.header2 ->
                    Toast.makeText(this, item.title, Toast.LENGTH_LONG).show()
                R.id.header3 ->
                    Toast.makeText(this, item.title, Toast.LENGTH_LONG).show()
            }
            true
        })
        popupMenu.show()
    }


}






