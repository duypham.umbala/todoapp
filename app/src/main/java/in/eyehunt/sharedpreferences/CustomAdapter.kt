package `in`.eyehunt.sharedpreferences

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView


class CustomAdapter() : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {


    var values: List<String> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var listener: OnClickItemClickListener? = null


    override fun getItemCount() = values.size

    override fun onCreateViewHolder(p0: ViewGroup, vp1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.task_layout, p0, false)
        return ViewHolder(v)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(values[position])

    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textView: CheckBox? = null
        var buttonRemove: ImageView? = null

        init {
            textView = itemView.findViewById(R.id.itemRadio) as CheckBox
            buttonRemove = itemView.findViewById(R.id.buttonRemove) as ImageView
        }

        fun bindData(item: String) {
            textView!!.text = item
            textView!!.setOnClickListener {
                listener?.onClickCheckedItem(item)
                textView!!.isChecked = false
            }
            buttonRemove!!.setOnClickListener {
                listener?.onClickRemveItem(item)
            }

        }
    }

}