package `in`.eyehunt.sharedpreferences

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class ModelList(
        @PrimaryKey
        var item: String? = null,
        var isPick: Boolean ?= true
) : RealmObject() {}


