package `in`.eyehunt.sharedpreferences

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_home.*

class Home : AppCompatActivity() {
    val adapterViewHome= AdapterHome()
    private lateinit var realm: Realm
    val adapterUnChecked = CustomAdapter()
    val adapterChecked = AdapterCheckedItem()
    private val viewNew: TextView
        get() = findViewById(R.id.textViewItem)
    private val listViewNew: ListView
        get() = findViewById(R.id.recyclerViewChild)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.commitTransaction()
        recyclerViewChild.apply {
            layoutManager = LinearLayoutManager(this@Home)
            adapter = adapterViewHome
        }

        todoDetail.setOnClickListener{
            val intentTodo = Intent(this@Home , MainActivity::class.java)
            startActivity(intentTodo)
        }
        showData()
    }

    fun getData(realmInstance: Realm): List<ModelList> {
        return realmInstance.where(ModelList::class.java).findAll()
    }

    @SuppressLint("SetTextI18n")
    fun showData() {
        val listUnChecked = mutableListOf<String>()
        val listChecked = mutableListOf<String>()

        getData(realm).forEach {
            if (it.isPick!!) {
                listChecked.add(it.item!!)
            } else {
                listUnChecked.add(it.item!!)
            }
        }
        adapterViewHome.valuesCheckedHome = listUnChecked
        adapterChecked.valuesChecked = listChecked

        if (listChecked.size <= 1) {
            when (listChecked.size) {
                0 -> viewNew.text = ""
                1 -> viewNew.text = "+ 1 Checked Item"
            }
        } else {
            viewNew.text = "+ ${listChecked.size}" + " Checked Items"
        }


    }
}
