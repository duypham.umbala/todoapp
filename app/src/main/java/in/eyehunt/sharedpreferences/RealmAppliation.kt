package `in`.eyehunt.sharedpreferences

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class RealmAppliation: Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val config = RealmConfiguration.Builder()
                .schemaVersion(42)
                .deleteRealmIfMigrationNeeded().build()
        Realm.setDefaultConfiguration(config)
    }
}