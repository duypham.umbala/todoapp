package `in`.eyehunt.sharedpreferences

interface OnClickItemClickListener {

    fun onClickRemveItem(item: String)
    fun onClickCheckedItem(item: String)
    fun onClickUnCheckedItem(item: String)
}