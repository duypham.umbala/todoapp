package `in`.eyehunt.sharedpreferences

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdapterHome() : RecyclerView.Adapter<AdapterHome.ViewHolder>() {

    var valuesCheckedHome: List<String> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = valuesCheckedHome.size

    override fun onCreateViewHolder(p0: ViewGroup, vp1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.view_home, p0, false)
        return ViewHolder(v)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var viewImage: CheckBox? = null
        var viewDataHome: TextView? =null

        init {
            viewDataHome = itemView.findViewById(R.id.viewHome) as TextView

        }

        fun bindDataChecked(item: String) {
            viewDataHome!!.text = item
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindDataChecked(valuesCheckedHome[position])
    }

}